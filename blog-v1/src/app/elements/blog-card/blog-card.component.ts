import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { BlogViewDataService } from '../../services/blog-view-data.service';


@Component({
  selector: 'ws-blog-card',
  templateUrl: './blog-card.component.html',
  styleUrls: ['./blog-card.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BlogCardComponent implements OnInit {

  constructor(private viewDataService: BlogViewDataService) {
  }

  ngOnInit() {
  }

}
