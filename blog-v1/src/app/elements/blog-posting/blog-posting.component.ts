import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { BlogViewDataService } from '../../services/blog-view-data.service';


@Component({
  selector: 'ws-blog-posting',
  templateUrl: './blog-posting.component.html',
  styleUrls: ['./blog-posting.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BlogPostingComponent implements OnInit {

  constructor(private viewDataService: BlogViewDataService) {
  }

  ngOnInit() {
  }

}
