import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ws-blog-page',
  templateUrl: './blog-page.component.html',
  styleUrls: ['./blog-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BlogPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
