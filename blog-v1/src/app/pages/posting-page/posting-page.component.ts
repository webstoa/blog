import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ws-posting-page',
  templateUrl: './posting-page.component.html',
  styleUrls: ['./posting-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PostingPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
